# Traitement sur les éléments d'un tableau

## Objectif

Ecrire une page PHP permettant de calculer les pourcentage de répartition des noms de domaines par fournisseur d'accès.

Vous pourrez utiliser le jeu de données suivant :
```
$tab = array(
    "php7@free.fr",
    "sacha8@gmail.com",
    "ariel3@wanadoo.fr",
    "webmestre@wanadoo.fr",
    "marcelduchamp9@gmail.com",
    "picasso69@gmail.com",
    "vangogh6@gmail.com",
    "matis63@free.fr",
    "degas45@wanadoo.fr"
);
```

Avec ce tableau, la page doit afficher (son style CSS est libre) :
```
Fournisseur d'accès : free.fr = 22.22 %
Fournisseur d'accès : gmail.com = 44.44 %
Fournisseur d'accès : wanadoo.fr = 33.33 %
```

### Indices sur le développement

- Essayez d'extraire les noms de domaine pour chacune des chaînes de caractères. La fonction [`explode(string $separator, string $string): array`](https://www.php.net/manual/fr/function.explode.php) peut être utilisée.
- Une fois les noms de domaine extraits, vous pouvez compter leur nombre. La fonction [`array_count_values(array $array): array`](https://www.php.net/manual/en/function.array-count-values.php) peut vous intéresser.
- Le nombre total d'éléments d'un tableau peut être trouvé en utilisant la fonction [`array_count_values(array $array): array`](https://www.php.net/manual/en/function.array-count-values.php).

## Tester votre code
Commande utilisée pour lancer un serveur de développement PHP (à exécuter à la racine de votre projet) :
```bash
php -S localhost:8000 -d display_errors=1
```

Puis connectez vous à l'URL
```
localhost:8000
```